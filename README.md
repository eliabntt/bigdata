### Big Data

This repository contains the program developed by my group (thanks to [@iamalme](https://bitbucket.org/iamalme) and [@rig8f](https://bitbucket.org/rig8f)) for the Big Data course in my M.Sc. degree.
We have used Spark within Java to develop programs mainly focused in the applications of the map-reduce paradigm.

In details we have developed:
 
 - HW1: operations with a random sequence of numbers
 - HW2: word counter
 - Hw3: clustering
 - HW4: clustering - advanced
 
 Aside to advance the our java's skills this work helped us to apply what was tought in class, giving us important results and at least a basic knowledge of this topic.