package it.unipd.dei.bdc1718;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class G10HM1 {

    /**
     * This function generates an ArrayList of Double values to use instead of the dataset
     *
     * @param num_values the number of values to be generated
     * @return the ArrayList of num_values doubles
     * @see ArrayList
     */

    private static ArrayList<Double> randomArrayList(int num_values) {
        ArrayList<Double> array = new ArrayList<>();

        for (int i = 0; i < num_values; i++) {
            Double temp = Math.random() * 100000;
            array.add(temp);
        }
        return array;
    }

    /**
     * This functions parses the content of the file given at program's start
     *
     * @param path the location of the file containing the data
     * @return an ArrayList of double numbers contained into the file
     * @throws FileNotFoundException if path is not found
     * @throws NumberFormatException if error in the file. Only double accepted
     * @throws Exception if the file is empty
     */

    private static ArrayList<Double> parseFile(String path) throws Exception {
        ArrayList<Double> array = new ArrayList<>();
        Scanner s = new Scanner(new File(path));
        if (!s.hasNext()) throw new Exception("Error: The file is empty");
        while (s.hasNext()) {
            if (s.hasNextDouble()) {
                Double val = Double.parseDouble(s.next());
                array.add(val);
            } else
                throw new NumberFormatException("Error in the file, only double(*.*) accepted");
        }
        s.close();
        return array;

    }

    /**
     * This is the comparator used to find the minimum using JavaRDD.min() function.
     * In order to be compatible with the min function in Spark, which works with a distributed filesystem,
     * it must implement the Serializable interface, otherwise the min function throws an exception because it
     * can't pass infos between the nodes.
     */

    private static class DoubleComparator implements Serializable, Comparator<Double> {
        @Override
        public int compare(Double o1, Double o2) {
            if (o1 < o2) return -1;
            else if (o2 < o1) return 1;
            return 0;
        }
    }

    /**
     * The main method of the class
     *
     * @param args expecting the path of the file with the double numbers
     * @throws FileNotFoundException if path is incorrect
     */

    public static void main(String[] args) throws FileNotFoundException {

        //Instructions to turn off useless logging in console
        Logger.getLogger("org").setLevel(Level.OFF);
        Logger.getLogger("akka").setLevel(Level.OFF);

        //Our list of numbers, which will be filled by the passed file's content or with random-generated values
        ArrayList<Double> lNumbers;

        //Catch exceptions and sort out in case of error
        if (args.length > 0) {
            try {
                System.out.println("Reading file: " + args[0]);
                lNumbers = parseFile(args[0]);
            } catch (Exception e) {
                System.out.println(e);
                System.out.println("Going for 1mln random samples");
                lNumbers = randomArrayList(1000000);
            }
        } else {
            //no argument passed
            System.out.println("No file passed, using 1mln random samples");
            lNumbers = randomArrayList(1000000);
        }

        //Setup Spark eliminating progress bars in output
        SparkConf conf = new SparkConf(true)
                .setAppName("Homework 1")
                .set("spark.ui.showConsoleProgress", "false");
        JavaSparkContext sc = new JavaSparkContext(conf);

        System.out.println("    Obtained " + lNumbers.size() + " values !");

        System.out.println("Using Map/Reduce to calculate average on lNumbers with sqrt(lNumbers.size()) buckets...");

        //Divide our numbers in sqrt(n) buckets
        int slices = (int) Math.sqrt(lNumbers.size());
        JavaRDD<Double> dNumbers = sc.parallelize(lNumbers, slices);
        //Can be done also with:
        //JavaRDD<Double> dNumbers = sc.parallelize(lNumbers);

        //Calculate average with Spark
        Double avg = dNumbers.reduce((x, y) -> (x + y)) / dNumbers.count();
        System.out.println("    Average: " + avg);
        System.out.println("Calculating distances using Map/Reduce...");

        //Calculate the RDD with the module of the difference between each value of dNumbers and the average
        JavaRDD<Double> dDiffavgs = dNumbers.map((x) -> (Math.abs(x - avg)));
        System.out.println("    Done!");
        System.out.println("    Computed " + dDiffavgs.count() + " distances!");


        System.out.println("Calculate iterative minimum...");

        //We calculate the iterative minumum without using Spark to verify Spark results are correct
        List<Double> tempList = dDiffavgs.collect();
        Double ref_min = tempList.get(0);
        for (int i = 1; i < tempList.size(); i++) {
            Double candidate = tempList.get(i);
            if (candidate < ref_min)
                ref_min = candidate;
        }

        System.out.println("    Reference min: " + ref_min);

        System.out.println("Now calculating the minimum with Spark...");
        System.out.println("1 - Map/Reduce");

        //Obtain the current timestamp to calculate computation time
        long start = System.currentTimeMillis();

        //Use map-reduce approach to find minimum
        Double min = dDiffavgs.reduce((x, y) -> (x < y) ? x : y);
        System.out.println("    Minimum: " + min);
        //Do difference between current time stamp and the previous one to find computation time
        System.out.println("    Time: " + (System.currentTimeMillis() - start) / (float) 1000 + " s");

        //Use JavaRDD.min() + comparator to find minimum
        System.out.println("2 - Using min + comparator function");
        start = System.currentTimeMillis();

        //Calculate minimum as just described
        min = dDiffavgs.min(new DoubleComparator());
        System.out.println("    Minimum: " + min);
        System.out.println("    Time: " + (System.currentTimeMillis() - start) / (float) 1000 + " s");

        //Chosen free statistic #1: count elements in the dataset > average
        System.out.println("Now count the elements > average...");
        start = System.currentTimeMillis();

        /*
           Calculate what we just described:
           - Apply a filter function to obtain a filtered dNumbers with values > average.
           - Map each value to 1 and count every "1" to obtain the result.
         */
        JavaRDD<Double> dNumbers2 = dNumbers.filter((x) -> (x > avg));
        int sum = dNumbers2.map((x) -> (1)).reduce((x, y) -> (x + y));
        System.out.println("    Elements: " + sum);
        System.out.println("    Time: " + (System.currentTimeMillis() - start) / (float) 1000 + " s");


        //Chosen free statistic #2: find the max in the dNumbers JavaRDD
        start = System.currentTimeMillis();
        double maxCmp = dNumbers.max(new DoubleComparator());
        System.out.println("The maximum value using the same custom comparator is " + maxCmp);
        System.out.println("    Time: " + (System.currentTimeMillis() - start) / (float) 1000 + " s");
    }

}

