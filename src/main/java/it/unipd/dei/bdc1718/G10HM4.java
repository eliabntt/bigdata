package it.unipd.dei.bdc1718;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class G10HM4 {

    private static final Random rng = new Random();

    /**
     * A stub method for the square of the Euclidean distance with extra checks.
     *
     * @param X First vector (point).
     * @param Y Second vector (point).
     * @return The squared Euclidean distance between the two points.
     */
    private static double squaredDistance(Vector X, Vector Y) {
        assert (X.size() == Y.size());
        return Vectors.sqdist(X, Y);
    }

    /**
     * Implements the Farthest-First Traversal algorithm.
     *
     * @param P A set of points (as list of {@link Vector}).
     * @param k The number of centers to detect.
     * @return The set of detected centers.
     */
    private static ArrayList<Vector> kcenter(ArrayList<Vector> P, int k) {
        // Initialize the centers set
        ArrayList<Vector> C = new ArrayList<>();
        int size = P.size();

        // Insert first point at random
        int randP = rng.nextInt(size); // range = [0, P.size()-1]
        //insert the first center
        C.add(P.get(randP));

        // Initialize array of distances
        double[] dist = new double[size];
        for (int i = 0; i < dist.length; i++)
            dist[i] = squaredDistance(C.get(0), P.get(i));

        // Search for other centers
        int maxIndex;
        double maxDistance, thisDistance;
        for (int i = 0; i < k - 1; i++) {
            maxDistance = 0;
            maxIndex = -1;

            // Loop through all points
            for (int pIndex = 0; pIndex < size; pIndex++) {
                /* If the saved distance is bigger than the one w.r.t. the last center, update.
                 * This ensure we always have the min. distance from a point to all centers.
                 * Note that the min. distance between a point that is a center and a center is 0. */
                thisDistance = squaredDistance(C.get(i), P.get(pIndex));
                if (dist[pIndex] > thisDistance)
                    dist[pIndex] = thisDistance;
                if (dist[pIndex] > maxDistance) {
                    maxDistance = dist[pIndex];
                    maxIndex = pIndex;
                }
            }
            // Add the point to the center's set
            C.add(P.get(maxIndex));
        }
        return C;
    }

    /**
     * Sequential approximation algorithm based on matching.
     *
     * @param points A set of points (as list of {@link Vector}).
     * @param k      The number of centers to detect.
     */
    private static ArrayList<Vector> runSequential(final ArrayList<Vector> points, int k) {
        // Preliminary check
        final int n = points.size();
        if (k >= n)
            return points;

        ArrayList<Vector> result = new ArrayList<>(k);
        boolean[] candidates = new boolean[n];
        Arrays.fill(candidates, true);

        for (int iter = 0; iter < k / 2; iter++) {
            // Find the maximum distance pair among the candidates
            double maxDist = 0;
            int maxI = 0, maxJ = 0;

            for (int i = 0; i < n; i++)
                if (candidates[i])
                    for (int j = i + 1; j < n; j++)
                        if (candidates[j]) {
                            double d = Math.sqrt(Vectors.sqdist(points.get(i), points.get(j)));
                            if (d > maxDist) {
                                maxDist = d;
                                maxI = i;
                                maxJ = j;
                            }
                        }

            // Add the points maximizing the distance to the solution
            result.add(points.get(maxI));
            result.add(points.get(maxJ));

            // Remove them from the set of candidates
            candidates[maxI] = false;
            candidates[maxJ] = false;
        }

        // Add an arbitrary point to the solution, if k is odd.
        // In this case, the point is the first among all current candidates.
        if (k % 2 != 0)
            for (int i = 0; i < n; i++)
                if (candidates[i]) {
                    result.add(points.get(i));
                    break;
                }

        if (result.size() != k)
            throw new IllegalStateException("Result of the wrong size");

        return result;
    }

    /**
     * Solver of the Diversity-Maximization problem.
     *
     * @param pointsRDD The data structure containing points to analyze.
     * @param k         The number of centers which will be returned by every kCenter and runSequential call.
     * @param numBlocks The number of partitions.
     * @return A list of centers, approximate solution of the Diversity-Maximization problem.
     */
    private static ArrayList<Vector> runMapReduce(JavaRDD<Vector> pointsRDD, int k, int numBlocks) {
        // To ensure that we measure the time correctly
        pointsRDD.count();
        long start = System.currentTimeMillis();

        /* (a) Partition pointsRDD into numBlocks subsets;
         * (b) Extract k points from each subset by running the sequential Farthest-First Traversal
         *     (same algorithm implemented for Homework 3); */
        pointsRDD = pointsRDD.repartition(numBlocks);
        pointsRDD = pointsRDD.mapPartitions(s1 -> {
            ArrayList<Vector> elements = new ArrayList<>();
            s1.forEachRemaining(elements::add);
            ArrayList<Vector> centers = kcenter(elements, k);
            return centers.iterator();
        });

        // Print the time taken by the coreset construction.
        pointsRDD.count();
        long end = System.currentTimeMillis();
        System.out.print(StringUtils.rightPad(Double.toString((end - start) / (double) 1000), 20));

        // (c) gather the numBlocks*k points extracted into coreset, an ArrayList<Vector>;
        ArrayList<Vector> coreset = new ArrayList<>(pointsRDD.collect());


        System.out.print(StringUtils.rightPad(Long.toString(coreset.size()), 20));
        start = System.currentTimeMillis();
        /* (d) return an ArrayList<Vector> object with k points determined by running the
         *     sequential max-diversity algorithm with coreset and k as inputs. */
        ArrayList<Vector> result = runSequential(coreset, k);

        /* Print the time taken by the computation of the final solution
         * (through the sequential algorithm) on the coreset. */
        end = System.currentTimeMillis();
        System.out.print(StringUtils.rightPad(Double.toString((end - start) / (double) 1000), 20));
        return result;
    }

    /**
     * Computes the average distance between all points (i.e., the sum of all
     * pairwise distances divided by the number of distinct pairs).
     *
     * @param pointsList The list of vectors (points).
     * @return The average distance between all points.
     */
    private static double measure(ArrayList<Vector> pointsList) {
        double average = 0;
        for (int i = 1; i < pointsList.size(); i++) {
            Vector pointToCompareWith = pointsList.get(i);
            for (int j = 0; j < i; j++)
                average += squaredDistance(pointToCompareWith, pointsList.get(j));
        }

        // since #elements = sum from i=1 to n-1 of[sum from j=0 to i-1 of(1)] = n*(n-1)/2
        return average / (0.5 * pointsList.size() * (pointsList.size() - 1));
    }

    /**
     * Starting point of the program.
     *
     * @param args Command line arguments.
     */
    public static void main(String[] args) {
        // Turn off useless logging in console
        Logger.getLogger("org").setLevel(Level.OFF);
        Logger.getLogger("akka").setLevel(Level.OFF);

        // Spark configuration
        SparkConf conf = new SparkConf(true)
                .setAppName("Homework 4")
                .setMaster("local[*]") // set local cluster
                .set("spark.ui.showConsoleProgress", "false");
        JavaSparkContext sc = new JavaSparkContext(conf);

        // Parameter number check
        if (args.length != 3 && args.length != 5) {
            System.out.println("Wrong parameters!" +
                    "\nCorrect syntax: G10HM4 [datasetPath] [k] [numBlocks]" +
                    "\nTo run batch operations: G10HM4 [datasetPath] [kMin] [kMax] [numBlocksMin] [numBlocksMax]");
            System.exit(1);
        }

        // Read and set variables
        String path = args[0];
        int k = -1, numBlocks = -1;
        int kMin = -1, kMax = -1, numBlocksMin = -1, numBlocksMax = -1;
        boolean batchMode = false;

        try {
            if (args.length == 3) {
                k = Integer.parseInt(args[1]);
                numBlocks = Integer.parseInt(args[2]);
                System.out.println("Using k = " + k + " and numBlocks = " + numBlocks);
            } else {
                kMin = Integer.parseInt(args[1]);
                kMax = Integer.parseInt(args[2]);
                numBlocksMin = Integer.parseInt(args[3]);
                numBlocksMax = Integer.parseInt(args[4]);
                batchMode = true;
                System.out.println("Using k = [" + kMin + "," + kMax + "] and numBlocks = [" + numBlocksMin + "," + numBlocksMax + "]");
            }
        } catch (NumberFormatException e) {
            System.out.println("Wrong parameters: k and numBlocks MUST be integers!");
            System.exit(1);
        }

        // Print whole test info and start
        System.out.println("Computation started!");
        System.out.println(StringUtils.rightPad("numBlocks value", 20) +
                StringUtils.rightPad("k value", 20) +
                StringUtils.rightPad("Coreset comp. time", 20) +
                StringUtils.rightPad("Coreset total size", 20) +
                StringUtils.rightPad("Div-Max comp. time", 20) +
                StringUtils.rightPad("Average", 20));

        // Decide whether to run in single or batch mode
        if (!batchMode) {
            // Print run info
            System.out.print(StringUtils.rightPad(Integer.toString(numBlocks), 20) +
                    StringUtils.rightPad(Integer.toString(k), 20));

            // Read the input points into a JavaRDD<Vector>
            JavaRDD<Vector> pointsRDD = InputOutput.readVectors(sc, path).repartition(numBlocks).cache();
            // Determine the solution of the diversity-maximization problem
            ArrayList<Vector> divMaxOutput = runMapReduce(pointsRDD, k, numBlocks);

            // Print the average distance among the solution points
            double avgDistance = measure(divMaxOutput);
            System.out.print(StringUtils.rightPad(Double.toString(avgDistance), 20));
        } else {
            // Batch routine to get results varying k and numBlocks
            for (int i = numBlocksMin; i <= numBlocksMax; i++)
                for (int j = kMin; j <= kMax; j++) {
                    // Print specific run information
                    System.out.print(StringUtils.rightPad(Integer.toString(i), 20) +
                            StringUtils.rightPad(Integer.toString(j), 20));

                    JavaRDD<Vector> pointsRDD = InputOutput.readVectors(sc, path).repartition(i).cache();

                    ArrayList<Vector> divMaxOutput = runMapReduce(pointsRDD, j, i);

                    double avgDistance = measure(divMaxOutput);
                    System.out.print(StringUtils.rightPad(Double.toString(avgDistance), 20) + "\n");
                }
        }
        System.out.println("\nComputation finished!");
    }
}
