package it.unipd.dei.bdc1718;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.io.IOException;
import java.util.*;

public class G10HM2 {
    private static String resultFormat = "%-20s %10d\n"; // string format to have well-tabled results
    private static String wordDelimiter = " "; // String.split() delimiter
    // other good delimiters are [\\W] (any non-word character) and [\\s+} (every sequence of 1+ spaces)

    /**
     * Main, starting method for the evaluation of several versions of the word count primitive
     *
     * @param args Arguments to the program
     * @throws IOException Only when viewing Spark's WebUI
     */
    public static void main(String[] args) throws IOException {
        // turn off useless logging in console
        Logger.getLogger("org").setLevel(Level.OFF);
        Logger.getLogger("akka").setLevel(Level.OFF);

        // content file's name: defaults to sample test file,
        // but the file name can be passed as program's first argument
        String fileName = "text-sample.txt";
        if (args.length == 1)
            fileName = args[0];
        else
            System.out.println("There could be too many or too few argument. I'm expecting the file(with the path).");

        // how many of the most used words to show: input by user
        int numToShow = 1;
        Scanner s = new Scanner(System.in);
        boolean check = false;
        do {
            try {
                System.out.print("How many most frequent words to show? ");
                numToShow = Integer.parseInt(s.next());
                check = true;
            } catch (Exception e) {
                System.out.println("Insert an integer please");
            }
        } while (!check);

        // Spark configuration
        SparkConf conf = new SparkConf(true)
                .setAppName("Homework 2")
                .setMaster("local[*]") // set local cluster
                .set("spark.ui.showConsoleProgress", "false");
        JavaSparkContext sc = new JavaSparkContext(conf);

        // read documents and cache them in memory
        // to prevent multiple reloading
        JavaRDD<String> docs = null;
        try {
            long start = System.currentTimeMillis();
            docs = sc.textFile(fileName).cache();
            docs.count(); // just to have previous op. performed and get real profiling time
            System.out.println("Time to load: " + (System.currentTimeMillis() - start) / (float) 1000 + " s");
        } catch (Exception e) {
            // cannot continue with errors on main content file, exit.
            System.out.println("Error loading text file, " + e.getMessage());
            System.exit(1);
        }

        // a quick analysis with detectBestConfiguration method (below) showed how,
        // by applying the word count methods over an RDD slitted in increasing number of partitions,
        // the fastest results are obtained using as number the number of CPU cores
        int numPartitions = Runtime.getRuntime().availableProcessors() * 4;

        // run every word count method; they return the result RDD
        // and the computation time encapsulated into this object
        ComputationResult result;

        System.out.println("\nUsing Improved Word Count 1: ");
        result = ImpWordCount1(docs, numPartitions);
        System.out.println("Time: " + result.time / (float) 1000 + " s");
        printResults(result.rdd, numToShow);

        System.out.println("\nUsing Improved Word Count 2: ");
        result = ImpWordCount2(docs, numPartitions);
        System.out.println("Time: " + result.time / (float) 1000 + " s");
        printResults(result.rdd, numToShow);

        System.out.println("\nUsing ReduceByKey: ");
        result = ReduceByKey(docs, numPartitions);
        System.out.println("Time: " + result.time / (float) 1000 + " s");
        printResults(result.rdd, numToShow);

        System.out.println("\nUsing alternativeReduceByKey: ");
        result = alternativeReduceByKey(docs, numPartitions);
        System.out.println("Time: " + result.time / (float) 1000 + " s");
        printResults(result.rdd, numToShow);

        //System.in.read(); // to inspect Spark's WebUI
    }

    /**
     * Print values that appear most frequently in the documents
     *
     * @param rdd     JavaPairRDD processed by a word count method
     * @param howMany How much words to display
     */
    private static void printResults(JavaPairRDD<String, Long> rdd, int howMany) {
        // no operations with 0 elements to show
        if (howMany < 1)
            return;

        // swap key and value for each pair to sort by frequency number (value)
        JavaPairRDD<Long, String> invertedRdd = rdd
                .mapToPair((x) -> (new Tuple2<>(x._2, x._1)))
                .sortByKey(false);

        System.out.println("----------- Results -----------");
        invertedRdd.take(howMany).forEach((x) -> System.out.format(resultFormat, x._2, x._1));
    }

    /**
     * First improved version of the WordCount primitive
     *
     * @param input   Documents RDD
     * @param numPart Number of partitions to use to split the input
     * @return PairRDD with frequencies for every word
     */
    private static ComputationResult ImpWordCount1(JavaRDD<String> input, int numPart) {
        // repartition with equally spaced sets
        JavaRDD<String> docs = input.repartition(numPart).cache();
        docs.count();
        long start = System.currentTimeMillis();

        /* Map phase:
         * for each Di, the map function produces one pair (w, ci(w))
         * for each word w ∈ Di, where ci(w) is the number of occurrences of w in Di. */
        JavaPairRDD<String, Long> map1 = docs
                .flatMapToPair((document) -> {
                    String[] words = document.split(wordDelimiter);
                    HashMap<String, Long> countMap = new HashMap<>();
                    for (String w : words) {
                        // check if an update or an addition is required
                        if (countMap.containsKey(w))
                            countMap.put(w, countMap.get(w) + 1);
                        else
                            countMap.put(w, 1L);
                    }

                    // move the HashMap to an ArrayList of Tuple2
                    ArrayList<Tuple2<String, Long>> output = new ArrayList<>();
                    for (Map.Entry<String, Long> item : countMap.entrySet())
                        output.add(new Tuple2<>(item.getKey(), item.getValue()));

                    return output.iterator();
                });

        /* Reduce phase:
         * For each word w, gather all intermediate pairs (w, 1) and produce the pair (w, c(w))
         * by summing all values (1’s) of these pairs. */
        JavaPairRDD<String, Long> reduce1 = map1
                .reduceByKey((x, y) -> (x + y));

        reduce1.count(); // to have previous op. performed (and thus real profiling time), unused value
        long delta = System.currentTimeMillis() - start;
        return new ComputationResult(reduce1, delta);
    }

    /**
     * Second improved version of the WordCount primitive
     *
     * @param input   Documents RDD
     * @param numPart Number of partitions to use to split the input
     * @return PairRDD with frequencies for every word
     */
    private static ComputationResult ImpWordCount2(JavaRDD<String> input, int numPart) {
        // repartition with equally spaced sets
        JavaRDD<String> docs = input.repartition(numPart).cache();
        docs.count();

        // get total word count N to split in sqrt(N) intermediate sets
        long start = System.currentTimeMillis();
        long wordCount = docs
                .map((document) -> (long) document.split(wordDelimiter).length)
                .reduce((x, y) -> (x + y));
        long sqrtWordCount = (long) Math.sqrt(wordCount);

        /* Round 1; Map phase:
         * for each document Di, produce the intermediate pairs (x, (w, ci(w))),
         * one for every word w ∈ Di, where x (the key of the pair) is a random value in
         * [0,√N) and ci(w) is the number of occurrences of w in Di */
        JavaPairRDD<Long, Tuple2<String, Long>> map1 = docs
                .flatMapToPair((document) -> {
                    String[] words = document.split(wordDelimiter);
                    ArrayList<Tuple2<Long, Tuple2<String, Long>>> output = new ArrayList<>();
                    HashMap<String, Long> countMap = new HashMap<>();

                    for (String w : words) {
                        // check if an update or an addition is required
                        if (countMap.containsKey(w))
                            countMap.put(w, countMap.get(w) + 1);
                        else
                            countMap.put(w, 1L);
                    }
                    // move the HashMap to an ArrayList of Long,Tuple2
                    for (Map.Entry<String, Long> entry : countMap.entrySet()) {
                        // random set selection between 0 and sqrt(N) for the tuple
                        long randomValue = (long) (Math.random() * (sqrtWordCount - 1));
                        Tuple2<Long, Tuple2<String, Long>> tuple = new Tuple2<>(randomValue, new Tuple2<>(entry.getKey(), entry.getValue()));
                        output.add(tuple);
                    }

                    return output.iterator();
                });

        /* Reduce phase:
         * For each key x gather all pairs (x, (w, ci(w))), and for each word w
         * occurring in these pairs produce the pair (w, c(x, w)) where
         * c(x, w) = sum [(x,(w,ci(w)))] of ci(w). Now, w is the key for (w, c(x, w)). */
        JavaPairRDD<String, Long> reduce1 = map1
                .groupByKey()
                .flatMapToPair((x) -> {
                    Iterable<Tuple2<String, Long>> values = x._2;
                    ArrayList<Tuple2<String, Long>> output = new ArrayList<>();
                    HashMap<String, Long> countMap = new HashMap<>();

                    for (Tuple2<String, Long> tuple : values) {
                        // check if an update or an addition is required
                        if (countMap.containsKey(tuple._1))
                            countMap.put(tuple._1, countMap.get(tuple._1) + tuple._2);
                        else
                            countMap.put(tuple._1, tuple._2);
                    }

                    // move the HashMap to an ArrayList of Tuple2
                    for (Map.Entry<String, Long> item : countMap.entrySet())
                        output.add(new Tuple2<>(item.getKey(), item.getValue()));

                    return output.iterator();
                });

        /* Round 2; Map phase: identity (do nothing)
         * Reduce phase:
         * for each word w, gather the at most √ N pairs (w, c(x, w)) resulting at
         * the end of the previous round, and produce the pair (w, sum [x] of c(x, w)) */
        JavaPairRDD<String, Long> reduce2 = reduce1
                .reduceByKey((x, y) -> (x + y));

        reduce2.count(); // to have previous op. performed (and thus real profiling time), unused value
        long delta = System.currentTimeMillis() - start;
        return new ComputationResult(reduce2, delta);
    }

    /**
     * Simple original version that uses reduceByKey method
     *
     * @param input   Documents RDD
     * @param numPart Number of partitions to use to split the input
     * @return PairRDD with frequencies for every word
     */
    private static ComputationResult ReduceByKey(JavaRDD<String> input, int numPart) {
        // repartition with equally spaced sets
        JavaRDD<String> docs = input.repartition(numPart).cache();
        docs.count();
        long start = System.currentTimeMillis();

        /* Map phase:
         * create for each word a pair (word, 1)
         * Reduce phase:
         * for each key gather all values of word counts and produce a pair
         * (word, sum of words) */
        JavaPairRDD<String, Long> wordcounts = docs
                .flatMapToPair((document) -> {
                    String[] tokens = document.split(wordDelimiter);
                    ArrayList<Tuple2<String, Long>> pairs = new ArrayList<>();
                    for (String token : tokens)
                        pairs.add(new Tuple2<>(token, 1L));

                    return pairs.iterator();
                })
                .reduceByKey((x, y) -> (x + y));

        wordcounts.count(); // to have previous op. performed (and thus real profiling time), unused value
        long delta = System.currentTimeMillis() - start;
        return new ComputationResult(wordcounts, delta);
    }

    /**
     * Alternative version of ReduceByKey method using asList in an intermediate step
     * between flatMap and mapToPair
     *
     * @param input   Documents RDD
     * @param numPart Number of partitions to use to split the input
     * @return PairRDD with frequencies for every word
     */
    private static ComputationResult alternativeReduceByKey(JavaRDD<String> input, int numPart) {
        JavaRDD<String> docs = input.repartition(numPart).cache();
        docs.count();
        long start = System.currentTimeMillis();
        JavaPairRDD<String, Long> counts = docs
                .flatMap(s1 -> Arrays.asList(s1.split(wordDelimiter)).iterator())
                .mapToPair(word -> new Tuple2<>(word, 1L))
                .reduceByKey((a, b) -> a + b);

        counts.count(); // to have previous op. performed (and thus real profiling time), unused value
        long delta = System.currentTimeMillis() - start;
        return new ComputationResult(counts, delta);
    }

    /**
     * Used to test many configurations searching for the partition number
     * that yields best time results for all versions of the word count primitive
     *
     * @param docs The initial dccuments' RDD
     */
    private static void detectBestConfiguration(JavaRDD<String> docs) {
        // containers for best results
        int bestWC1 = 1;
        int bestWC2 = 1;
        int bestWC3 = 1;
        int bestWC4 = 1;
        long bestTimeWC1 = Long.MAX_VALUE;
        long bestTimeWC2 = Long.MAX_VALUE;
        long bestTimeWC3 = Long.MAX_VALUE;
        long bestTimeWC4 = Long.MAX_VALUE;
        int max = 32; // max num partitions for the tests

        for (int c = 1; c <= max; c++) {
            System.out.print("Part: " + c + " >>> ");
            long delta1 = ImpWordCount1(docs, c).time;
            if (delta1 < bestTimeWC1) {
                bestWC1 = c;
                bestTimeWC1 = delta1;
            }
            long delta2 = ImpWordCount2(docs, c).time;
            if (delta2 < bestTimeWC2) {
                bestWC2 = c;
                bestTimeWC2 = delta2;
            }
            long delta3 = ReduceByKey(docs, c).time;
            if (delta3 < bestTimeWC3) {
                bestWC3 = c;
                bestTimeWC3 = delta3;
            }
            long delta4 = alternativeReduceByKey(docs, c).time;
            if (delta4 < bestTimeWC3) {
                bestWC4 = c;
                bestTimeWC4 = delta4;
            }
            System.out.println(String.format("%d %d %d %d", delta1, delta2, delta3, delta4));
        }
        System.out.println("\nWordCount1: best part #: " + bestWC1 + " , time: " + bestTimeWC1);
        System.out.println("WordCount2: best part #: " + bestWC2 + " , time: " + bestTimeWC2);
        System.out.println("ReduceByKey: best part #: " + bestWC3 + " , time: " + bestTimeWC3);
        System.out.println("altReduceByKey: best part #: " + bestWC4 + " , time: " + bestTimeWC4);
    }
}

/**
 * Container for results of methods execution: data and total time needed
 */
class ComputationResult {
    JavaPairRDD<String, Long> rdd;
    long time;

    ComputationResult(JavaPairRDD<String, Long> rdd, long time) {
        this.rdd = rdd;
        this.time = time;
    }
}
