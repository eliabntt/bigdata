package it.unipd.dei.bdc1718;

import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

public class G10HM3 {

    /* We have decided to put a seed in the random number generator
     * so we can have reproducible results that depend only on the
     * implementation and in the parameters (weights, k and k1) and
     * not also in the random number generation. */
    private static Random rng = new Random(10L);

    /**
     * A stub method for the euclidean distance with extra checks.
     *
     * @param X First vector (point)
     * @param Y Second vector (point)
     * @return The euclidean distance between the two points
     */
    private static double euclideanDistance(Vector X, Vector Y) {
        assert (X.size() == Y.size());
        return Vectors.sqdist(X, Y);
    }

    /**
     * A stub method for the squared euclidean distance.
     *
     * @param X First point
     * @param Y Second point
     * @return The square of the euclidean distance between the two points
     */
    private static double squaredDistance(Vector X, Vector Y) {
        return Math.pow(euclideanDistance(X, Y), 2);
    }

    /**
     * Implements the Farthest-First Traversal algorithm.
     *
     * @param P A set of points (as list of {@link Vector})
     * @param k The number of centers to detect
     * @return The set of detected centers
     */
    private static ArrayList<Vector> kcenter(ArrayList<Vector> P, int k) {

        // initialize the centers set
        ArrayList<Vector> C = new ArrayList<>();
        int size = P.size();

        // insert first point at random
        int randP = rng.nextInt(size); // range = [0, P.size()-1]

        //insert the first center
        C.add(P.get(randP));

        // initialize array of distances
        double[] dist = new double[size];
        for (int i = 0; i < dist.length; i++)
            dist[i] = euclideanDistance(C.get(0), P.get(i));

        // search for other centers
        int maxIndex;
        double maxDistance, thisDistance;
        for (int i = 0; i < k - 1; i++) {
            maxDistance = 0;
            maxIndex = -1;

            // loop through all points
            for (int pIndex = 0; pIndex < size; pIndex++) {
                /* If the saved distance is bigger than the one w.r.t. the last center, update.
                 * This ensure we always have the min. distance from a point to all centers.
                 * Note that the min. distance between a point that is a center and a center is 0. */
                thisDistance = euclideanDistance(C.get(i), P.get(pIndex));
                if (dist[pIndex] > thisDistance)
                    dist[pIndex] = thisDistance;
                if (dist[pIndex] > maxDistance) {
                    maxDistance = dist[pIndex];
                    maxIndex = pIndex;
                }
            }
            // add the point to the center's set
            C.add(P.get(maxIndex));
        }
        return C;
    }

    /**
     * Implements a weighted variant of the kmeans++ algorithm.
     *
     * @param P  A set of points
     * @param WP A set of weights
     * @param k  The number of center to compute
     * @return The set of detected centers
     */
    private static ArrayList<Vector> kmeansPP(ArrayList<Vector> P, ArrayList<Long> WP, int k) {
        // initialize the centers set
        ArrayList<Vector> C = new ArrayList<>();
        int size = P.size();

        // insert first point at random
        int randP = rng.nextInt(size); // range = [0, P.size()-1]
        C.add(P.get(randP));

        /* prepare the array to keep the distances
         * note: contrary to what is done in the kcenter function,
         * here initialization is performed later inside the loop */
        double[] dist = new double[size];
        double sumDQSq;

        // search for all next centers
        for (int i = 1; i < k; i++) {
            sumDQSq = 0;

            // computation of sum(dq^2) and of the list of dp^2
            Vector p;
            long weight;
            double thisDistance;
            for (int pIndex = 0; pIndex < size; pIndex++) {
                p = P.get(pIndex);
                weight = WP.get(pIndex);
                thisDistance = euclideanDistance(C.get(i - 1), p);

                // update distances if less than saved distance
                if (i > 1) {
                    if (thisDistance < dist[pIndex])
                        dist[pIndex] = thisDistance;
                } else {
                    // first initialization
                    dist[pIndex] = thisDistance;
                }

                sumDQSq += Math.pow(dist[pIndex], 2) * weight;
            }

            /* Use a list to contain Pij elements, keep track of the sum
             * to check it's a good probability distribution.
             * We can avoid to use the sumDQSq without changing the results
             * since it's just a normalization factor. Removing this means
             * that in our case the sum of pij will not give 1 anymore.
             * Note that avoiding normalization will save a number of
             * elementary operations in the next for loop and in the
             * previous one, respectively updating sumDQSq and doing the division. */
            double sum = 0, pij;
            double[] PI = new double[size];
            for (int pIndex = 0; pIndex < size; pIndex++) {
                weight = WP.get(pIndex);
                pij = Math.pow(dist[pIndex], 2) * weight / sumDQSq;
                PI[pIndex] = pij;
                sum += pij;
            }
            assert (sum == 1.0);

            // draw a random number
            double rndValue = rng.nextDouble();

            // search the index j for that it holds
            // sum_1^(r-1) pij <= rndValue <= sum_1^r pij
            int piIndex = 0;
            double sumInf = -1, sumSup = PI[0];
            while (!((sumInf <= rndValue) && (rndValue <= sumSup))) {
                piIndex++;
                assert (piIndex < PI.length);
                sumInf = sumSup;
                sumSup += PI[piIndex];
            }

            // the element at the index is the next center
            C.add(P.get(piIndex));
        }
        return C;
    }

    /**
     * The kmeans objective function for given points and centers.
     *
     * @param P A set of points
     * @param C A set of centers
     * @return The average squared distance of p in P from its closest center
     */
    private static double kmeansObj(ArrayList<Vector> P, ArrayList<Vector> C) {
        double average = 0;
        int Psize = P.size();
        int Csize = C.size();

        // loop through through each point in P
        for (Vector p : P) {
            // get the minimum among the distances from each center
            // that is, the distance from its nearest center
            double dist, minDist = squaredDistance(p, C.get(0));
            for (int cIndex = 1; cIndex < Csize; cIndex++) {
                dist = squaredDistance(p, C.get(cIndex));
                if (dist < minDist)
                    minDist = dist;
            }
            average += minDist;
        }

        // divide by the number of points in P
        average /= Psize;
        return average;
    }

    /**
     * Starting point of the program
     *
     * @param args Array of initial parameters
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        ArrayList<Vector> input = null;
        int k = -1, k1 = -1;
        long start;

        // use command line parameters to get input points and k values
        if (args.length == 3) {
            try {
                System.out.println("Reading vectors...");
                start = System.currentTimeMillis();
                input = InputOutput.readVectorsSeq(args[0]);
                System.out.println(" - Done in " + (System.currentTimeMillis() - start) / 1000.0 + " s");
                k = Integer.parseInt(args[1]);
                k1 = Integer.parseInt(args[2]);
            } catch (IOException ex) {
                System.out.println("ERROR reading from file");
                ex.printStackTrace();
            } catch (NumberFormatException ex) {
                System.out.println("ERROR parsing k values");
                ex.printStackTrace();
            } catch (Exception ex) {
                System.out.println("Unexpected behaviour on input, see stack trace to get more infos.");
                ex.printStackTrace();
            }
        } else {
            System.out.println("Wrong or no input parameters for the program, trying interactive input.");
        }

        // go interactive for some parameters if necessary
        if (input == null) {
            boolean done = false;
            while (!done) {
                System.out.print("Insert input points' file name: ");
                String filename = in.nextLine();
                try {
                    System.out.println("Reading vectors...");
                    start = System.currentTimeMillis();
                    input = InputOutput.readVectorsSeq(filename);
                    System.out.println(" - Done in " + (System.currentTimeMillis() - start) / 1000.0 + " s");
                    done = true;
                } catch (IOException ignored) {
                    System.out.println("ERROR reading file");
                } catch (IllegalArgumentException e) {
                    System.out.println("This program requires an input file, not a directory");
                }
            }
        }

        if (k == -1) {
            boolean done = false;
            while (!done) {
                System.out.print("Insert value for k: ");
                try {
                    k = in.nextInt();
                    if (k > 0 && k < input.size())
                        done = true;
                } catch (NumberFormatException ignored) {
                    System.out.println("ERROR parsing number");
                }
            }
        }

        if (k1 == -1) {
            boolean done = false;
            while (!done) {
                System.out.print("Insert value for k1: ");
                try {
                    k1 = in.nextInt();
                    if (k1 > 0 && k1 < input.size())
                        done = true;
                } catch (NumberFormatException ignored) {
                    System.out.println("ERROR parsing number");
                }
            }
        }

        // Start elaborations
        // 1
        System.out.println("\nRunning kCenter on the input, with k = " + k + "...");
        start = System.currentTimeMillis();
        kcenter(input, k); // discard output
        System.out.println(" - Done in " + (System.currentTimeMillis() - start) / 1000.0 + " s");

        /* Weights for kMeans++ are set by default all equal to 1.
         * If we set all of them equal to another number there is obviously no change in the output.
         * An idea could be to use some intrinsic feature of the vector to generate weights.
         * With a generation based on the vector norm (see below) we can generally see
         * improvements in most cases, but also a very bad result with a large number of points.
         *
         * Here there are some results with k=10 and k1=20, L2 norm.
         * Legend: objective function for kMeans++: on the full set / on the centers set
         **************************************************
         *      FILE      *   GENERATED   *     ALL 1s    * IMPROVEMENTS?
         **************************************************
         * vecs-50-10000  * 26.72 / 50.25 * 18.83 / 51.54 * only using centers set
         * vecs-50-50000  * 20.20 / 54.39 * 21.08 / 54.42 * always
         * vecs-50-100000 * 24.10 / 47.56 * 25.29 / 47.59 * always
         * vecs-50-500000 * 23.29 / 52.84 * 22.18 / 54.67 * only using centers set
         **************************************************
         *
         * Here there are some results with k=10 and k1=20, L1 norm.
         **************************************************
         *      FILE      *   GENERATED   *     ALL 1s    * IMPROVEMENTS?
         **************************************************
         * vecs-50-10000  * 19.06 / 50.26 * 18.83 / 51.54 * only using centers set
         * vecs-50-50000  * 21.24 / 53.89 * 21.08 / 54.42 * only using centers set
         * vecs-50-100000 * 24.29 / 47.53 * 25.29 / 47.59 * always
         * vecs-50-500000 * 21.54 / 1672  * 22.18 / 54.67 * very bad on centers set
         **************************************************
         */

        ArrayList<Long> weights;
        weights = new ArrayList<>(Collections.nCopies(input.size(), 1L));

        boolean with_norm = false;
        int norm = 1;
        /* // alternative weights generation, with vector norm
        weights = new ArrayList<>(input.size());
        for (int i = 0; i < input.size(); i++) {
            weights.add((long) Vectors.norm(input.get(i), norm));
            with_norm = true;
        }*/
        String weightsStr = with_norm ? " L" + norm + " norm..." : "1...";

        // 2
        System.out.println("\nRunning kMeans++ on the input, with k = " + k + " and all weights = " + weightsStr);
        start = System.currentTimeMillis();
        ArrayList<Vector> centers = kmeansPP(input, weights, k);
        System.out.println(" - Done in " + (System.currentTimeMillis() - start) / 1000.0 + " s");

        System.out.println("Computing the objective function value over the previously detected centers");
        start = System.currentTimeMillis();
        double ofValue = kmeansObj(input, centers);
        System.out.println(" - Done in " + (System.currentTimeMillis() - start) / 1000.0 + " s");
        System.out.println("Objective function value: " + ofValue);

        // 3
        System.out.println("\nRunning kCenter on the input, with k1 = " + k1 + "...");
        start = System.currentTimeMillis();
        ArrayList<Vector> kcCenters = kcenter(input, k1); // discard output
        System.out.println(" - Done in " + (System.currentTimeMillis() - start) / 1000.0 + " s");

        System.out.println("Running kMeans++ on the center set, with k = " + k + " and all weights = " + weightsStr);
        start = System.currentTimeMillis();
        centers = kmeansPP(kcCenters, weights, k);
        System.out.println(" - Done in " + (System.currentTimeMillis() - start) / 1000.0 + " s");

        System.out.println("Computing the objective function value over the input and kMeans++ centers");
        start = System.currentTimeMillis();
        ofValue = kmeansObj(input, centers);
        System.out.println(" - Done in " + (System.currentTimeMillis() - start) / 1000.0 + " s");
        System.out.println("Objective function value: " + ofValue);
    }
}
